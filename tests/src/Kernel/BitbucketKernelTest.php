<?php

namespace Drupal\Tests\league_oauth_login_bitbucket\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\league_oauth_login_bitbucket\Plugin\LeagueOauthLogin\Bitbucket;

/**
 * Tests the basic functionality of the module.
 *
 * @group league_oauth_login_bitbucket
 */
class BitbucketKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_bitbucket',
  ];

  /**
   * Test that we can construct the plugin.
   */
  public function testPluginConstruct() {
    $bitbucket = Bitbucket::create($this->container, [], 'bitbucket', []);
    $this->assertInstanceOf(Bitbucket::class, $bitbucket);
  }

}
