<?php

namespace Drupal\Tests\league_oauth_login_bitbucket\Kernel;

use Drupal\Tests\league_oauth_login\Kernel\LoginTestBase;

/**
 * Class LoginTest to test log in.
 *
 * @group league_oauth_login_bitbucket
 */
class LoginTest extends LoginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $providerId = 'bitbucket';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_bitbucket',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getProviderPath() {
    return 'https://bitbucket.org/site/oauth2/authorize';
  }

}
