<?php

namespace Drupal\league_oauth_login_bitbucket\Plugin\LeagueOauthLogin;

use Drupal\league_oauth_login\LeagueOauthLoginPluginBase;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Stevenmaguire\OAuth2\Client\Provider\Bitbucket as BitbucketProvider;

/**
 * Slack login.
 *
 * @LeagueOauthLogin(
 *   id = "bitbucket",
 *   label = @Translation("Bitbucket"),
 *   description = @Translation("Bitbucket login.")
 * )
 */
class Bitbucket extends LeagueOauthLoginPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getAuthUrlOptions() {
    return [
      'scope' => ['email', 'repository'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return new BitbucketProvider([
      'clientId' => $this->configFactory->get('league_oauth_login_bitbucket.settings')->get('clientId'),
      'clientSecret' => $this->configFactory->get('league_oauth_login_bitbucket.settings')->get('clientSecret'),
      'redirectUri' => $this->configFactory->get('league_oauth_login_bitbucket.settings')->get('redirectUri'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserName(ResourceOwnerInterface $owner) {
    /** @var \Stevenmaguire\OAuth2\Client\Provider\BitbucketResourceOwner $owner */
    return $owner->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail(ResourceOwnerInterface $owner, $access_token) {
    /** @var \Stevenmaguire\OAuth2\Client\Provider\Bitbucket $provider */
    $provider = $this->getProvider();
    $req = $provider->getAuthenticatedRequest($provider::METHOD_GET, 'https://api.bitbucket.org/2.0/user/emails', $access_token);
    $res = $provider->getParsedResponse($req);
    if (empty($res["values"][0]["email"])) {
      throw new \Exception('No email address found');
    }
    return $res["values"][0]["email"];
  }

}
